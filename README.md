# 项目介绍

Smartinsight项目包括三大子系统：

- 数据元收集---问卷调研系统（其中一种获取数据方式，就是这里的问卷调研系统）
- 大数据分析和处理系统
- 数据结果分析与行业研究展示系统

问卷调研系统：
      前端分三部分： 安卓app移动端，IOSapp移动端，web浏览器                     端，reactjs +bootstrap+ flux+ react-ruoter框架搭建
      后台：nodejs + Mongodb起服务与存储

[原项目 Gitee](https://gitee.com/git_zliang/SmartinsightXiangMuWenJuanDiaoYanZiXiTong)

包含两大部分，移动app（安卓，IOS）和web端，后台node服务，具备扩展性部署。

## 前后台技术栈

- 前台： reactjs  + Flux + react-router + ES5/ES6 +Ajax +Bootstrap +browserify打包+各种第三方包（见项目代码使用）
- 后台： nodejs + express + Mongodb +acl

## 开发环境

- node安装（5.9-6.10版本）
- MongoDB安装
- git安装
- Python2.7安装
- vs2010安装
- webstrom安装
- chrome前台调试

## 项目框架结构目录

- 后台相关目录：
  - access目录              ------ 后台服务acl用户资源权限控制model
  - doc                     ------ 项目设计文档
  - app.js                  ------ 后台服务启动文件
  - db.js                   ------ 连接MongoDB文件
  - dict.js                 ------ 一些类似宏变量定义文件
  - logger.js               ------ 日志存储
  - package.json            ------ 项目配置文件
  - model                   ------ web端，app端接口文件
  - uploads                 ------ 下载上传文件资源服务端存放目录
  - .......
- 前端相关目录：
  - js                      ------ 前端代码
  - public                  ------ css html 图片等静态文件
  - .......

## 下载运行

git clone https://gitee.com/pauli/smartinsight.git

cd smartinsight

npm install

npm install bower -g

bower install bootstrap

npm run build

或

npm run build-dist

打开 MongoDB

启动服务 : node app.js

后台命令注册超级管理员：
curl -d "password=xxxxxx" http://localhost:8080/createsuperadmin/

浏览器访问：http://localhost:8080/public/#/ 用户名 superadmin 密码 （刚刚设置的 xxxxxx ）

## 演示效果

1. 登录界面

功能：官方网站链接Link，此网站干什么FAQ子界面,匿名建议反馈Feedback子界面，登录Login子组件,注册Register子组件，扫码安装app

![登录界面](https://git.oschina.net/uploads/images/2017/0802/164014_1c1f6f65_1447612.jpeg "1501662391(1).jpg")

1.1 反馈子界面

![反馈子界面](https://git.oschina.net/uploads/images/2017/0802/164817_3a675b94_1447612.jpeg "1501663712(1).jpg")

1.2 注册界面

![注册界面](https://git.oschina.net/uploads/images/2017/0802/165236_8bc8cc65_1447612.jpeg "1501663967(1).jpg")

2. 主页

功能：九大子管理模块，分别是组织管理，工作人员管理，问卷管理，问卷详细编辑管理，广告推广及客户端版本管理，设置模块，元数据模块，反馈建议管理，问卷样板模块。

![主页](https://git.oschina.net/uploads/images/2017/0802/172128_47797fc2_1447612.jpeg "1501665681(1).jpg")

3. 组织界面

功能：创建组织，及组织管理员（组织之间不关联，无数据交互，组织管理员属于二级管理员，拥有自己组织下所有权限）
![组织界面](https://git.oschina.net/uploads/images/2017/0802/172237_fa593112_1447612.jpeg "1501665776(1).jpg")

4. 工作人员模块界面

功能：超级管理员创建属于无组织人员，亦或叫匿名人员调研，组织管理员创建工作人员（属于组织内工作人员，与其它组织不关联）
![工作人员界面](https://git.oschina.net/uploads/images/2017/0802/172640_15b4c372_1447612.jpeg "1501665988(1).jpg")

5. 问卷模块

功能：问卷列表，问卷删除，编辑，克隆成模板，分享等几大功能模块

![问卷管理界面](https://git.oschina.net/uploads/images/2017/0802/173740_2f0c5062_1447612.jpeg "1501666673(1).jpg")

5.1 问卷结果统计自动分析界面

![问卷结果](https://git.oschina.net/uploads/images/2017/0802/174449_f6b9cf80_1447612.jpeg "1501666883(1).jpg")

6. 问卷详情编辑界面

功能：各种问卷题型创建编辑，提交与审批等等

![详情编辑界面](https://git.oschina.net/uploads/images/2017/0802/174108_6731c3a4_1447612.jpeg "1501666883(1).jpg")
